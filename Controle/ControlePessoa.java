package Agenda.Controle;
import Agenda.Modelo.Pessoa;
import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }
    

// métodos
   
    public void adicionar(Pessoa umaPessoa) {
		listaPessoas.add(umaPessoa);
    }

    public void remover(Pessoa umaPessoa) {
        listaPessoas.remove(umaPessoa);
    }
    
    public Pessoa pesquisarNome(String umNome) {
        for (Pessoa umaPessoa: listaPessoas) {
            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
        }
        return null;
    }
    
    public void exibirContatos () {
        for (Pessoa umaPessoa : listaPessoas) {
            System.out.println(umaPessoa.getNome());
        }
    }

}